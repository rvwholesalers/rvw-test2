<?php


use PHPUnit\Framework\TestCase;

class FormattersTest extends TestCase
{
    public function testFormatPhone()
    {
        $f = new Formatters();

        $result = $f->FormatPhone('9378439000');
        $this->assertEquals('(937) 843-9000', $result);

        $result = $f->FormatPhone('937-843-9000');
        $this->assertEquals('(937) 843-9000', $result);

        $result = $f->FormatPhone('937 843 9000');
        $this->assertEquals('(937) 843-9000', $result);

        $result = $f->FormatPhone('937.843.9000');
        $this->assertEquals('(937) 843-9000', $result);
    }

    public function testPluralize()
    {
        $f = new Formatters();

        $result = $f->Pluralize('Dog', 0);
        $this->assertEquals('Dogs', $result);

        $result = $f->Pluralize('Dog', 1);
        $this->assertEquals('Dog', $result);

        $result = $f->Pluralize('Dog', 2);
        $this->assertEquals('Dogs', $result);

        $result = $f->Pluralize('Pony', 1);
        $this->assertEquals('Pony', $result);

        $result = $f->Pluralize('Pony', 2);
        $this->assertEquals('Ponies', $result);

        $result = $f->Pluralize('Horse', 1);
        $this->assertEquals('Horse', $result);

        $result = $f->Pluralize('Horse', 2);
        $this->assertEquals('Horses', $result);
    }
}
